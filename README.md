# Basic react unit test settings

use vite build react with jest

## Library

- vite
- jest
- @testing-library/jest-dom
- @testing-library/react
- @testing-library/user-event
- ts-jest
- @types/jest
- ts-node
- identity-obj-proxy : mock css
- jest-environment-jsdom
